<?php
/**
 * Created by IntelliJ IDEA.
 * User: thomasgros
 * Date: 31/01/2018
 * Time: 16:23
 */

namespace App\Controller;

use App\Entity\Product;
use App\Form\ProductType;
use App\Service\ProductService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Illustre un controller Symfony Classique dans le cadre d'une application type CRUD.
 *
 * Les méthodes du controlleur sont en charge de produire une réponse HTTP.
 * Typiquement elles vont
 * - être invoquées par le mécanisme de routing détectant les routes annotées @Route
 * - 1) vérifier les paramètres de route si besoin, et fail fast (er: retourner une 404 par exemple)
 * - 2) exécuter de la logique métier via l'appel à des services
 *      - les services sont injectés comme paramètres de méthodes. Symfony exploite les type-hint php
 * - 3) retourner une réponse HTTP en invoquant Twig par la méthode render
 *
 * @package App\Controller
 */
class ProductController extends Controller
{

    /**
     * @Route("/products/list", name="products_list")
     * @return Response
     */
    function list(ProductService $productService) {
        $products = $productService->getProducts();

        return $this->render('product/list.html.twig', [
            'products' => $products
        ]);
    }

    /**
     * @Route("/products/detail/{id}", requirements={"id"="\d+"})
     * @return Response
     */
    function detail($id, ProductService $productService) {
        $product = $productService->getProductById(intval($id));

        if($product === null) {
           throw $this->createNotFoundException("Product Not Found");
        }

        return $this->render('product/detail.html.twig', [
            'product' => $product
        ]);
    }

    /**
     * Cette méthode est un peu plus compliquée que les autres.
     * Une bonne pratique de gestion des formulaire est d'avoir un unique endpoint (ici /products/new)
     * qui gère l'affichage et le traitement du formulaire.
     *
     * L'affichage se fait via une première requête HTTP GET. A ce moment handleRequest détecte
     * que le formulaire n'a pas été soumis et le code se contente de retourner la réponse HTTP contenant le formulaire
     *
     * Le traitement se fait par la soumission du formulaire via HTTP POST. A ce moment handleRequest
     * détecte qu'un formulaire a été soumis, valide le formulaire.
     * Si le formulaire est valide, on procède au traitement métier puis on redirige si le traitement s'est
     * bien déroulé. Note: ce n'est pas illustré ici mais un service peut jeter des exceptions qu'il faut catcher afin de retourner des réponses HTTP adéquates
     *
     * En cas d'échec du traitement du formulaire, on utilise twig pour afficher le formulaire avec les messages d'erreur
     * @Route("/products/new")
     * @return Response
     */
    function new(Request $request, ProductService $productService) {
        $product = new Product();
        $form = $this->createForm(ProductType::class, $product);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $product = $form->getData();
            $productService->createProduct($product->getTitle(), $product->getPrice());
            return $this->redirectToRoute('products_list');
        }

        return $this->render('product/new.html.twig', [
            'form' => $form->createView()
        ]);
    }
}