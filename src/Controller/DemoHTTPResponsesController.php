<?php
/**
 * Created by IntelliJ IDEA.
 * User: thomasgros
 * Date: 29/01/2018
 * Time: 11:27
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Cette classe illustre quelques mécanismes de réponse de base fournis par Symfony4
 * @package App\Controller
 */
class DemoHTTPResponsesController extends Controller
{

    /**
     * Réponse HTTP 200 OK contenant du HTML
     * @return Response
     */
    function hello() {
        $response = new Response('<html><body><p>coucou</p><img src="https://httpbin.org/image/png"></body></html>');
        return $response;
    }

    /**
     * Réponse HTTP 200 OK contenant du HTML, illustrant lien de page a à page b
     * @return Response
     */
    function a() {
        $response = new Response('<html><body><a href="b">lien vers b</a></body></html>');
        return $response;
    }

    /**
     * Réponse HTTP 200 OK contenant du HTML, illustrant lien de page b à page a
     * @return Response
     */
    function b() {
        $response = new Response('<html><body><a href="a">lien vers a</a></body></html>');
        return $response;
    }

    /*
     * Illustre la manière de retourner une réponse HTTP 200 contenant un document arbitraire
     * autre que HTML, ici du text/plain, sans passer par un template
     * Les routes sont définies dans config/routes.yaml
     */
    function textplain() {
        $response = new Response('<p>blah blah text plain</p>', 200, ['Content-Type' => 'text/plain']);
        return $response;
    }


    /****************************************************************************
     * En complément de la formation,
     * d'autres exemples de réponses avec routes annotées
     ****************************************************************************/

    /**
     * Illustre la manière de retourner une réponse HTTP 200 contenant du HTML,
     * sans passer par un template
     * @Route("/200-OK")
     */
    public function standard200OkResponse() {
        return new Response('<html><body>everythings\'s fine</body></html>');
    }

    /**
     * Illustre la manière de retourner une réponse HTTP 200 contenant un document arbitraire
     * autre que HTML, ici CSS, sans passer par un template
     * @Route("/custom-response")
     */
    public function custom200OkResponse() {
        // https://symfony.com/doc/current/controller.html#the-request-and-response-object
        $response = new Response(); // voir également le constructeur
        $response->setStatusCode(200);
        $response->setContent('p { color: red }');
        $response->headers->set('Content-Type', 'text/css');


        return $response;
    }

    /**
     * Illustre la manière de retourner une réponse 404 NOT FOUND
     * @Route("/404-NOT-FOUND")
     */
    public function error400NotFound() {
        throw $this->createNotFoundException('Oups... resource not found');
    }

    /**
     * Illustre la manière de retourner une réponse JSON
     * @Route("/json", name="json")
     */
    public function jsonResponse() {
        // il est possible de retourner une réponse JSON en encodant nous même la chaine de caractères
        // $response = new Response('{"msg":"hello", "code":123}', 200, ['Content-Type' => 'application/json']);
        // return $response;

        // ou alors en héritant de Controller et en utilisant la méthode json qui crée la représentation pour nous
        // https://symfony.com/doc/current/controller.html#returning-json-response
        return $this->json(array('id' => 'some id'));
    }

    /**
     * Illustre la manière de retourner une document binaire, ici un fichier vidéo
     * @Route("/download", name="videos_download")
     */
    public function download() {
        // https://symfony.com/doc/current/controller.html#streaming-file-responses

        // récupérer des videos de demo sur http://www.sample-videos.com



        // load the file from the filesystem
        // $file = new File('/path/to/some_file.pdf');

        // return $this->file($file);

        // rename the downloaded file
        // return $this->file($file, 'custom_name.pdf');

        // display the file contents in the browser instead of downloading it
        $path = '/Users/thomasgros/Development/Trainings/Symfony/Symfony4-01-2018/videos/SampleVideo_1280x720_1mb.mp4';
        return $this->file($path, 'SampleVideo_1280x720_1mb.mp4', ResponseHeaderBag::DISPOSITION_INLINE);
    }
}