<?php
/**
 * Created by IntelliJ IDEA.
 * User: thomasgros
 * Date: 02/02/2018
 * Time: 15:40
 */

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

/**
 * Ce mécanisme permet de définir un formulaire réutilisable facilement à plusieurs endroits
 * (comprendre plusieurs controllers/templates)
 * @package App\Form
 */
class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('price')
            ->add('save', SubmitType::class);
    }
}