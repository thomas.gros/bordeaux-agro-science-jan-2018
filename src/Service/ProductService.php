<?php
/**
 * Created by IntelliJ IDEA.
 * User: thomasgros
 * Date: 01/02/2018
 * Time: 14:25
 */

namespace App\Service;

use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Les services peuvent représenter ds opérations métiers ou des opérations transversales.
 *
 * Généralement je préfère mettre en place des services plutôt que d'écrire directement
 * la logique métier dans les controllers
 *
 * @package App\Service
 */
class ProductService
{
    private $em;

    /**
     * ProductService constructor.
     * Les services peuvent être dépendant d'autres serviceS.
     * Ici l'EntityManager par défaut est autowiré par Symfony
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }


    /**
     * Retourne la liste des produits
     * @return array contenant la liste des produits. Le tableau est vide si il n'y a pas de produits.
     */
    function getProducts() {
        return $this
                ->em
                ->getRepository('App\Entity\Product')
                ->findAll();
    }

    /**
     * Retourne un produit en fonction de son identifiant
     * @param $id l'identifiant du produit recherché
     * @return Product correspondant au produit recherché. null si aucun produit n'a l'identifiant demandé.
     */
    function getProductById($id) {
        return $this
                ->em
                ->getRepository('App\Entity\Product')
                ->find($id);
    }

    function createProduct($title, $price) {
        $product = new Product();
        $product->setTitle($title);
        $product->setPrice($price);

        $this->em->persist($product);
        $this->em->flush();
    }
}