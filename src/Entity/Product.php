<?php
/**
 * Created by IntelliJ IDEA.
 * User: thomasgros
 * Date: 01/02/2018
 * Time: 10:48
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * Plusieurs approches sont possibles, ici démonstration d'une entité 'anémique'
 * contenant juste de l'information (état) et des accesseurs (getter/setter)
 *
 * Les annotations sont utilisées par Doctrine pour faire le pont entre le le monde objet et le
 * monde relationel.
 *
 * Pour que Doctrine reconnaisse une classe comme une entité il faut à minima
 * - annoter la classer @ORM\Entity
 * - annoter un champ comme clé primaire via @ORM\Id
 *
 * L'entité Product est associée à une table product (pour changer l'association, annoter l'entité @ORM\Table)
 *
 *
 * @ORM\Entity
 */
class Product
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $title;

    /**
     * @ORM\Column(type="decimal")
     */
    private $price;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price): void
    {
        $this->price = $price;
    }


}